![image](https://i.giphy.com/media/f5GyIBXJ3L0DS/giphy.webp)
# Start initialize project ( Develop )( prepare for staging)

### Deveplopment tools
- docker (https://docs.docker.com/install/)
- docker compose (https://docs.docker.com/compose/install/)
- npm (https://docs.npmjs.com/cli/install)
- nodemon (https://www.npmjs.com/package/nodemon)
### NPM Dependencies libarary
- mongoose (https://www.npmjs.com/package/mongoose)
- joi (https://www.npmjs.com/package/joi)
- express (https://www.npmjs.com/package/express)
- config (https://www.npmjs.com/search?q=config)
- jsonwebtoken (https://www.npmjs.com/package/jsonwebtoken)

### System Initialize

```
npm install
./docker-start.sh
```
---

## Template query
  
  
### User
  
#### Structure
  
```json
{
    "type": "admin",
    "image": "no-image",
    "_id": "5bb07d9def0bfb00b593a102",
    "email": "exsample0000_demo@email.test.com",
    "password": "90bd7fd4f52bd4d40b3fe7cb2a1021ea",
    "created_user": "admin",
    "created_date": "2018-09-30T07:39:09.052Z",
    "updated_date": "2018-09-30T07:39:09.052Z"
}
```
  
#### GET
  
<HOST>/api/user
  
<HOST>/api/user/<EMAIL>
  
```
127.0.0.1:3030/api/user/exsample_user@email.test.com
```
  
#### DELETE
<HOST>/api/user/<_ID>
  
```
127.0.0.1:3030/api/user/5bb07408742f460011bc2120
```
  
#### POST
<HOST>/api/user
  
__Type:__
  
admin  
cook  
doctor  
medical-record  
nutritionist  
  
__Payload:__
```json
    {
        "type": "admin",
        "image": "no-image",
        "email": "exsample_user@email.test.com",
        "password": "exsample_password_123",
        "created_user": "admin",
        "updated_user":"admin"
    }
```
  
#### PUT
  
<HOST>/api/user
  
__Payload:__
```json
{
    "id": "5b9b803d59f93603e61dadce",
    "by": "admin",
    "data": {
        "email": "10_update_data@email.test.com",
        "password": "changeOne",
        "type": "nutritionist"
    }
}
```
  
---
### Room
  
#### Structure
  
```json
{
    "room_id": 101,
    "patient_id": null,
    "patient": null,
    "admit_date": null,
    "_id": "5bb9e47016c015025eda9bf9",
    "floor": 1,
    "created_user": "admin",
    "updated_user": "admin",
    "created_date": "2018-10-07T10:48:16.982Z",
    "updated_date": "2018-10-07T10:48:16.982Z"
}
```
  
#### GET
  
<HOST>/api/room
  
<HOST>/api/room/<ROOM_ID>
  
```
127.0.0.1:3030/api/room/101
```
  
#### DELETE
<HOST>/api/room/<_ID>
  
```
127.0.0.1:3030/api/room/5bb07408742f460011bc2120
```
  
#### POST
  
<HOST>/api/room
  
__Payload:__
```json
    {
        "room_id": 101,
        "patient_id": "5b9b803d59f93603e61dadce",
        "patient": "Mr. Sumchai Madee",
        "floor": 1,
        "created_user": "admin",
        "updated_user": "admin"
    }
```
  
#### PUT
  
<HOST>/api/room
  
```json
{
    "id": "5b9b803d59f93603e61dadce",
    "by": "admin",
    "data": {
        "room_id": 101,
        "patient_id": 100001,
        "patient": "Mr. Sumchai Madee",
        "floor": 1
    }
}
```
  
---
### Patient
  
#### Structure
  
```json
 {
        "disease": [
            1001,
            1002,
            1003
        ],
        "_id": "5bbe0c40e11304029a36329d",
        "room_id": 201,
        "name": "Mr. Metong Madee",
        "image": "no-image",
        "doctor": "Dr. Sumchai Pitack",
        "created_user": "admin",
        "updated_user": "admin2",
        "admit_date": "2018-10-10T14:27:12.218Z",
        "created_date": "2018-10-10T14:27:12.218Z",
        "updated_date": "2018-10-10T14:43:23.873Z",
        "patient_id": 1
}
```
  
#### GET
  
<HOST>/api/patient
  
<HOST>/api/patient/<PATIENT_NAME>
  
```
127.0.0.1:3030/api/room/Mr.%20Sumchai%20Madee
```
  
#### DELETE
<HOST>/api/patient/<_ID>
  
```
127.0.0.1:3030/api/patient/5bbe0c40e11304029a36329d
```
  
#### POST
  
<HOST>/api/patient
  
__Payload:__
```json
{
    "room_id": 201,
    "name": "Mr. Sumboon Madee",
    "image": "no-image",
    "disease": [1004, 1002, 1003],
    "doctor": "Dr. Sumchai Pitack",
    "created_user": "admin",
    "updated_user": "admin"
}
```
  
#### PUT
  
<HOST>/api/room
  
```json
{
    "id": "5bbe0c40e11304029a36329d",
    "by": "admin2",
    "data": {
        "room_id": 201,
        "name": "Mr. Metong Madee",
        "image": "no-image",
        "disease": [1001, 1002, 1003],
        "doctor": "Dr. Metong Pitack",
        "updated_user": "admin2"
    }
}
```
  
---
### Disease
  
#### Structure
  
```json
    {
        "_id": "5bdf6f1985eaf80011299757",
        "disease_id": 1,
        "name_th": "โรคเบาหวาน",
        "name_en": "Diabetes",
        "ingre_heal": [],
        "ingre_harm": [],
        "created_user": "admin",
        "updated_user": "admin",
        "created_date": "2018-11-04T22:13:45.247Z",
        "updated_date": "2018-11-04T22:13:45.247Z"
    }
```
#### GET
  
<HOST>/api/disease
  
<HOST>/api/disease/<DISEASE_NAME>
  
```
127.0.0.1:3030/api/disease/Diabetes
```
  
#### DEL
  
<HOST>/api/disease/<_ID>
  
```
127.0.0.1:3030/api/disease/5bbe0c40e11304029a36329d
```
  
#### POST
  
<HOST>/api/disease
  
__Payload:__
```json
    {
        "name_th": "โรคเบาหวาน",
        "name_en": "Diabetes",
        "ingre_heal": [],
        "ingre_harm": [
            "sugar"
        ],
        "created_user": "admin",
        "updated_user": "admin",
    }
```
  
#### PUT
  
<HOST>/api/disease
  
__Payload:__
```json
{
    "id": "5bdf6f1985eaf80011299757",
    "by": "admin",
    "data": {
        "disease_id": 1,
        "name_th": "โรคเบาหวาน",
        "name_en": "Diabetes",
        "ingre_heal": [],
        "ingre_harm": [
            "sugar"
        ],
        "updated_user": "admin"
    }
}
```
  
---
### Food
  
#### Structure
  
```json
    {
        "_id": "5bdfaa5f5ccac80011df834c",
        "food_id": 1,
        "name_th": "ข้าวผัดไข่",
        "name_en": "EggFriedRice",
        "image": "no-image",
        "type": "meat",
        "repast": "lunch",
        "ingredient": [],
        "created_user": "admin",
        "updated_user": "admin"
    }
```
#### GET
  
<HOST>/api/food
  
<HOST>/api/food/<FOOD_NAME>
  
```
127.0.0.1:3030/api/food
```
  
#### DEL
  
<HOST>/api/food/<_ID>
  
```
127.0.0.1:3030/api/food/5bbe0c40e11304029a36329d
```
  
#### POST
  
<HOST>/api/food
  
Repast : breakfast   lunch   dinner
  
Type   : meat   sweet
  
__Payload:__
```json
{
    "name_th": "ข้าวผัดไข่",
    "name_en": "EggFriedRice",
    "type": "meat",
    "repast": "lunch",
    "ingredient": [],
    "image": "no-image",
    "created_user": "admin",
    "updated_user": "admin"
}
```
  
#### PUT
  
<HOST>/api/food
  
__Payload:__
```json
{
    "id": "5bdfaa5f5ccac80011df834c",
    "by": "admin",
    "data":    {
        "food_id": 1,
        "name_en": "EggFriedRice",
        "image": "no-image",
        "ingredient": [],
        "name_th": "ข้าวผัดไข่",
        "type": "meat",
        "repast": "lunch"
    }
}
```
  
---
### Foodlist
  
#### Structure
  
```json
   {
        "_id": "5bdfc2f0ae1e76001de8820a",
        "foodlist_id": 1,
        "label": "Normal Meal",
        "routine": "2018-11-05T02:39:43.607Z",
        "menu": [
            {
                "food_id": 1,
                "name_en": "EggFriedRice",
                "image": "no-image",
                "ingredient": [],
                "_id": "5bdfaa5f5ccac80011df834c",
                "name_th": "ข้าวผัดไข่",
                "type": "meat",
                "repast": "lunch",
                "created_user": "admin",
                "updated_user": "admin",
                "updated_date": "2018-11-05T02:39:43.607Z"
            }
        ],
        "created_date": "2018-11-05T04:11:28.950Z",
        "updated_date": "2018-11-05T04:11:28.950Z",
        "updated_user": "admin",
        "created_user": "admin"
    }
```
#### GET
  
<HOST>/api/foodlist
  
<HOST>/api/foodlist/<FOODLIST_LABEL>
  
```
127.0.0.1:3030/api/foodlist/Normal%20Meal
```
  
#### DEL
  
<HOST>/api/foodlist/<_ID>
  
```
127.0.0.1:3030/api/foodlist/5bbe0c40e11304029a36329d
```
  
#### POST
  
<HOST>/api/foodlist
  
__Payload:__
```json
{
    "label": "Normal Meal",
    "menu": [],
    "routine": "2018-11-05T02:39:43.607Z",
    "updated_user": "admin",
    "created_user": "admin"
}
```
  
#### PUT
  
<HOST>/api/foodlist
  
__Payload:__
```json
{
    "id": "5bdfc2f0ae1e76001de8820a",
    "by": "admin",
    "data":{
        "_id": "5bdfc2f0ae1e76001de8820a",
        "label": "Normal Meal",
        "menu": [],
        "routine": "2018-11-05T02:39:43.607Z"
    }
}

```
  
---
### Ingredient
  
#### Structure
  
```json
{
    "_id": "5be0ec1bcf60fa001fd0c6d4",
    "name_th": "มะเขือ",
    "name_en": "egg plant",
    "created_user": "admin",
    "updated_user": "admin",
    "ingredient_id": 1
}
```
#### GET
  
<HOST>/api/ingredient
  
<HOST>/api/ingredient/<INGREDIENT_NAME>
  
```
127.0.0.1:3030/api/imngredient/egg%20plant
```
  
#### DEL
  
<HOST>/api/ingredient/<_ID>
  
```
127.0.0.1:3030/api/ingredient/5bbe0c40e11304029a36329d
```
  
#### POST
  
<HOST>/api/ingredient
  
__Payload:__
```json
{
    "name_th": "มะเขือ",
    "name_en": "egg plant",
    "created_user": "admin",
    "updated_user": "admin"
}
```
#### PUT
  
<HOST>/api/ingredient
  
__Payload:__
```json
{
    "id": "000000111111122222",
    "by": "admin",
    "data": {
        "name_th": "มะเขือ",
        "name_en": "egg plant",
        "created_user": "admin",
        "updated_user": "admin"
    }
}
```
---
### Auth
  
#### Post Structure
  
```json
{
    "email": "test@mail.com",
    "password": "textSecret123",
}
```
#### Set jwt in http header
x-auth-token
  
