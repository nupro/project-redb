#!/bin/sh
echo "Get start generate seed data ..."
curl -H 'Content-Type: application/json' -X GET http://127.0.0.1:3030/api/user/debug/seed
curl -H 'Content-Type: application/json' -X GET http://127.0.0.1:3030/api/disease/debug/seed
curl -H 'Content-Type: application/json' -X GET http://127.0.0.1:3030/api/ingredient/debug/seed
