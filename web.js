const config = require('config')
const express = require('express')
const session = require('express-session')
const jwt = require('jsonwebtoken')
const cors = require('cors')
const cookieParser = require('cookie-parser')
// const utf8 = require("utf8")

/* require router */
const login = require('./routes/web/login')

/* requier web middleware */
//const isLogin = require('./middleware/web/isLogin')

/* check configuration file */
// export NODE_ENV=develop
if (!config.get('sessPrivateKey')){
    process.exit(1)
}

const app = express()

/* Set Views */
// app.set('view engine', 'pug') // old view engine
app.set('view engine', 'ejs')
app.set('views', './views') // default view

/* Middleware */
app.use(express.json())
//app.use(express.urlencoded({ extended: true }))
app.use(cors()) // cross origin
app.use(cookieParser()) // parser cookie from request
app.use(express.static('public')) // set public path
app.use(session({
  secret: config.get('sessPrivateKey'),
  //cookie  : { maxAge  : new Date(Date.now() + (60 * 1000 * 30)) }
}))

/* web route path */
app.use('/login', login)

// check is login
app.use((req, res, next) => {
    console.log('session user : ')
    console.log(req.session.user)
    if(!req.session.user) res.redirect('/login')
    else next()
})

/*----- PAGES -----*/
app.get('/', (req, res) => {
    res.status(200).render('pages/main',
      {data: {jwt: req.session.jwt, user: req.session.user}
    })
})
app.get('/order-list', (req, res) => {
    res.status(200).render('pages/export')
})
app.get('/export-menu', (req, res) => {
    res.status(200).render('pages/menuList')
})
app.get('/setup', (req, res) => {
    if(req.query.mod == 'y') res.status(200).render('pages/debug')
})
app.get('/logout', (req, res, next)=>{
    req.session.user = null
    res.redirect('/login')
})
/*----- PARTIALS -----*/
app.get('/partials/user', (req, res) => {
    res.status(200).render('partials/user',
      {data: {jwt: req.session.jwt, user: req.session.user}
    })
})
app.get('/partials/food', (req, res) => {
    res.status(200).render('partials/food',
      {data: {jwt: req.session.jwt, user: req.session.user}
    })
})
app.get('/partials/patient', (req, res) => {
    res.status(200).render('partials/patient',
      {data: {jwt: req.session.jwt, user: req.session.user}
    })
})
app.get('/partials/disease', (req, res) => {
    res.status(200).render('partials/disease',
      {data: {jwt: req.session.jwt, user: req.session.user}
    })
})
app.get('/partials/ingredient', (req, res) => {
    res.status(200).render('partials/ingredient',
      {data: {jwt: req.session.jwt, user: req.session.user}
    })
})
app.get('/partials/foodlist', (req, res) => {
    res.status(200).render('partials/foodlist',
      {data: {jwt: req.session.jwt, user: req.session.user}
    })
})
app.get('/partials/foodcreate', (req, res) => {
    res.status(200).render('partials/food-create',
      {data: {jwt: req.session.jwt, user: req.session.user}
    })
})
// app.get('/hide/dashboard', (req, res) => {
//   res.status(200).render('partials/dashboard')
// })

const port = process.env.API_PORT | 80
app.listen(port, () => console.log(`Listening on port ${port}...`))
