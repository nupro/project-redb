const mongoose = require('mongoose')
/* MODEL */
const { Disease, validateDisease } = require('../../models/disease')
/* HELPER FUNCTION */
const { makeObjId } = require('../../helper/makeObjId')

/*----- SELLECT ALL CONTROLLER-----*/
exports.fetchAll = async () => {
    return await Disease.find().sort('disease_id')
}
/*----- SELLECT ALL ONLY NAME FIELD CONTROLLER-----*/
exports.fetchAllOnlyName = async () => {
    return await Disease.find({}, {'name_en':1, 'name_th':1}).sort('disease_id')
}
/*----- SELECT ONECE CONTROLLER -----*/
exports.fetchOnce = async (obj) => {
    return await Disease.findOne(obj)
}

/*----- DELETE ONCE CONTROLLER -----*/
exports.delOnce = async (obj) => {
    let funcError = false
    try {
        resultDel = await Disease.deleteOne(obj)
        funcError = (resultDel['n'] === 0) ? true : false
    } catch (error) {
        funcError = true
    }
    return funcError
}

/*----- CREATE CONTROLLER -----*/
/* simple model
{
    disease_id: 001,
    name_th: 'none',
    name_en: 'none',
    ingre_heal: [],
    ingre_harm: [],
    created_user: 'none',
    updated_user: 'none',
    created_date: Date.now(),
    updated_date: Date.now()
}
*/
exports.create = async (obj) => {
    let funcError = false
    try {
        const disease = new Disease(obj)
        const passObj = Disease
        const fieldId = 'disease_id'
        disease[fieldId] = (await makeObjId(passObj, fieldId)) + 1
        const { error, value } = validateDisease(disease)
        await disease.save()
    } catch (error) {
        funcError = true
        //console.log(error)
    }
    return funcError
}

/*----- UPDATE CONTROLLER -----*/
/* simple model
{
    "id": "000000111111122222",
    "by": "admin",
    "data": {
        disease_id: 001,
        name_th: 'none',
        name_en: 'none',
        ingre_heal: [],
        ingre_harm: [],
        created_user: 'none',
        updated_user: 'none',
        updated_date: Date.now()
    }
}
*/
exports.update = async (obj) => {
    funcError = (!(obj.hasOwnProperty('id') && obj.hasOwnProperty('by') && obj.hasOwnProperty('data'))) ? true : false
    if(funcError == false) {
        /* cache variable data */
        const id = obj['id']
        const updateBy = obj['by']
        const data = obj['data']
        try {
            data['updated_user'] = updateBy
            data['updated_date'] = Date.now()
            const {error, value} = validateDisease(data)
            const updateStatus = await Disease.updateOne({"_id": id}, value)
        } catch (error) {
            funcError = true
            console.log(error)
        }
        return funcError
    }
}

/* DEBUG FUNCTION */
exports.debugClear = async () => {
    await Disease.deleteMany()
}
const seedJson  = require('../../seeder/disease.json')
exports.debugSeed = async () => {
    for (seed of seedJson) {
      await this.create(seed)
    }
}
