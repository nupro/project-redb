const mongoose = require('mongoose')
/* MODEL */
const {Room, validateRoom, validateRoomUpdate} = require('../../models/room')
/* HELPER FUNCTION*/
const {makeObjId} = require('../../helper/makeObjId')

/*----- SELLECT ALL CONTROLLER-----*/
exports.fetchAll = async () => {
    const result = await Room.find().sort('room_id')
    return result
}

/*----- SELECT ONECE CONTROLLER -----*/
exports.fetchOnce = async (obj) => {
    return await Room.findOne(obj)
}

/*----- DELETE ONCE CONTROLLER -----*/
exports.delOnce = async (obj) => {
    let funcError = false
    try {
        resultDel = await Room.deleteOne(obj)
        funcError = (resultDel['n'] === 0) ? true : false
    } catch (error) {
        funcError = true
    }
    return funcError
}

/*----- CREATE CONTROLLER -----*/
/* simple model
{
    "room_id": 101,
    "patient_id": null,
    "patient": null,
    "floor": 1,
    "admit_date": null,
    "created_user": "admin",
    "updated_user": "admin",
    "created_date": Date.now(),
    "updated_date": Date.now()
}*/
exports.create = async (obj) => {
    let funcError = false
    try {
        obj['admit_date'] = (obj['patient_id'] !== null && obj['patient'] !== null) ? Date.now() : null
        const room = new Room(obj)
        const { error, value } = validateRoom(room)
        await room.save()
    } catch (error) {
        funcError = true
        //console.log(error)
    }
    return funcError
}

/*----- UPDATE CONTROLLER -----*/
/* simple model
{
    "room_id": 102,
    "patient_id": null,
    "patient": null,
    "floor": 4,
    "admit_date": null,
}*/
exports.update = async (obj) => {
    funcError = (!(obj.hasOwnProperty('id') && obj.hasOwnProperty('by') && obj.hasOwnProperty('data'))) ? true : false

    if(funcError == false) {
        /* cache variable data */
        const id = obj['id']
        const updateBy = obj['by']
        const data = obj['data']
        try {
            /*----- update by id -----*/
            data['admit_date'] = (data['patient_id'] !== null && data['patient'] !== null) ? Date.now() : null
            data['updated_user'] = updateBy
            data['updated_date'] = Date.now()
            const {error, value} = validateRoomUpdate(data)
            const updateStatus = await Room.updateOne({"_id": id}, value)
        } catch(error) {
            console.log(error)
            funcError = true
        }
    }
    return funcError
}

/* DEBUG FUNCTION */
exports.debugClear = async () => {
    await Room.deleteMany()
}
