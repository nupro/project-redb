const mongoose = require('mongoose')
/* MODEL */
const { User, validateUser, validateUserUpdate } = require('../../models/user')
/* HELPER FUNCTION */
const { userSeeder } = require('../../helper/userSeeder')
const { hashPass } = require('../../helper/hashPass')

mongoose.set('useCreateIndex', true)
//mongoose.set('useFindAndModify', false)

/*----- SELECT-ALL CONTROLLER -----*/
exports.fetchAll = async () => {
    return await User.find().sort({create_date: -1})
}

/*----- SELECT-ONCE -----*/
exports.fetchOnce = async (obj) => {
    return await User.findOne(obj)
}

/*----- DELETE-ONCE CONTROLLER -----*/
exports.delOnce = async (obj) => {
    let funcError = false
    try {
        resultDel = await User.deleteOne(obj)
        funcError = (resultDel['n'] === 0) ? true : false
    } catch (error) {
        funcError = true
    }
    return funcError
}

/*----- CREATE CONTROLLER -----*/
exports.create = async (obj) => {
    let funcError = false
    try {
        obj['password'] = hashPass(obj['password'])
        const user =  new User(obj)
        const { error, value } = validateUser(user)
        if (!error) await user.save()
        // await user.save()
    } catch (error) {
        funcError = true
        //console.log(error)
    }
    return funcError
}

/*----- UPDATE CONTROLLER -----*/
/* simple update model
 {
    "id": "5b9b803d59f93603e61dadce",
    "by": "creator",
    "data": {
        "email": "10_update_data@email.test.com",
        "password": "changeOne",
        "type": "nutritionist",
        "updated_user": "creator"
    }
}
*/
exports.update = async (obj) => {
    let funcError = false
    funcError = (!(obj.hasOwnProperty('id') && obj.hasOwnProperty('by') && obj.hasOwnProperty('data'))) ? true : false

    if(funcError == false) {
        /* cache variable data */
        const id = obj['id']
        const updateBy = obj['by']
        const data = obj['data']
        try {
            /*----- check old pass -----*/
            if (data.hasOwnProperty('password')){
                const oldPass = await User.findById(obj['id'], {'password': 1})
                if (oldPass['password'] !== data['password']) data['password'] = hashPass(data['password'])
            }
            /*----- update by id -----*/
            data['updated_user'] = updateBy
            data['updated_date'] = Date.now()
            const {error, value} = validateUserUpdate(data)
            const updateStatus = await User.updateOne({"_id": id}, value)
        } catch(error) {
            funcError = true
            console.log(error)
        }
    }
    return funcError
}

/* DEBUG FUNCTION */
exports.debugClear = async () => {
    await User.deleteMany()
}

exports.debugSeed = async () => {
    let userList = userSeeder()
    for (seed of userList) {
      console.log(seed)
      await this.create(seed)
    }
}
