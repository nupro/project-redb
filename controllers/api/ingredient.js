const mongoose = require('mongoose')
/* MODEL */
const { Ingredient, validateIngredient } = require('../../models/ingredient')
/* HELPER FUNCTION */
const { makeObjId } = require('../../helper/makeObjId')

/*----- SELLECT ALL CONTROLLER-----*/
exports.fetchAll = async () => {
    return await Ingredient.find().sort('ingredient_id')
}

/*----- SELECT ONECE CONTROLLER -----*/
exports.fetchOnce = async (obj) => {
    return await Ingredient.findOne(obj)
}

/*----- DELETE ONCE CONTROLLER -----*/
exports.delOnce = async (obj) => {
    let funcError = false
    try {
        resultDel = await Ingredient.deleteOne(obj)
        funcError = (resultDel['n'] === 0) ? true : false
    } catch (error) {
        funcError = true
    }
    return funcError
}

/*----- CREATE CONTROLLER -----*/
/* simple model
{
    infredient_id: 001,
    name_th: 'sugar',
    name_en: 'sugar',
    name_unit: 'g',
    created_user: 'admin',
    updated_user: 'admin',
    created_date: Date.now(),
    updated_date: Date.now()
}
*/
exports.create = async (obj) => {
    let funcError = false
    try {
        const ingredient = new Ingredient(obj)
        const passObj = Ingredient
        const fieldId = 'ingredient_id'
        ingredient[fieldId] = (await makeObjId(passObj, fieldId)) + 1
        const { error, value } = validateIngredient(ingredient)
        await ingredient.save()
    } catch (error) {
        funcError = true
        //console.log(error)
    }
    return funcError
}

/*----- UPDATE CONTROLLER -----*/
/* simple model
{
    "id": "000000111111122222",
    "by": "admin",
    "data": {
            infredient_id: 001,
            name_th: 'sugar',
            name_en: 'sugar',
            updated_user: 'admin',
            updated_date: Date.now()
    }
}
*/
exports.update = async (obj) => {
    funcError = (!(obj.hasOwnProperty('id') && obj.hasOwnProperty('by') && obj.hasOwnProperty('data'))) ? true : false
    if(funcError == false) {
        /* cache variable data */
        const id = obj['id']
        const updateBy = obj['by']
        const data = obj['data']
        try {
            data['updated_user'] = updateBy
            data['updated_date'] = Date.now()
            const {error, value} = validateIngredient(data)
            const updateStatus = await Ingredient.updateOne({"_id": id}, value)
        } catch (error) {
            funcError = true
            console.log(error)
        }
        return funcError
    }
}

/* DEBUG FUNCTION */
const seedJson = require('../../seeder/ingredient.json')
exports.debugClear = async () => {
    await Ingredient.deleteMany()
}
exports.debugSeed = async () => {
    for (seed of seedJson) {
      await this.create(seed)
    }
}
