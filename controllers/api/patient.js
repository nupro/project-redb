const mongoose = require('mongoose')
/* MODEL */
const { Patient, validatePatient, validatePatientUpdate } = require('../../models/patient')
/* HELPER FUNCTION */
const { makeObjId } = require('../../helper/makeObjId')

/*----- SEARCH CONTROLLER-----*/
exports.find = async (obj) => {
    const result = await Patient.find(obj)
    return result
}
/*----- SELLECT ALL CONTROLLER-----*/
exports.fetchAll = async () => {
    const result = await Patient.find().sort('patient_id')
    return result
}
/*----- SELECT ONECE CONTROLLER -----*/
exports.fetchOnce = async (obj) => {
    return await Patient.findOne(obj)
}
/*----- DELETE ONCE CONTROLLER -----*/
exports.delOnce = async (obj) => {
    let funcError = false
    try {
        resultDel = await Patient.deleteOne(obj)
        funcError = (resultDel['n'] === 0) ? true : false
    } catch (error) {
        funcError = true
    }
    return funcError
}
/*----- CREATE CONTROLLER -----*/
exports.create = async (obj) => {
    let funcError = false
    try {
        const patient = new Patient(obj)
        const passObj = Patient
        const fieldId = 'patient_id'
        patient[fieldId] = (await makeObjId(passObj, fieldId)) + 1
        console.log('----- show ------')
        console.log(patient)
        console.log('----- show ------')
        const { error, value } = validatePatient(patient)
        await patient.save()
    } catch (error) {
        console.log('----- ERROR ------')
        console.log(error)
        console.log('----- ERROR ------')
        funcError = true
    }
    return funcError
}
/*----- UPDATE CONTROLLER -----*/
exports.update = async (obj) => {
    funcError = (!(obj.hasOwnProperty('id') && obj.hasOwnProperty('by') && obj.hasOwnProperty('data'))) ? true : false
    if(funcError == false) {
        /* cache variable data */
        const id = obj['id']
        const updateBy = obj['by']
        const data = obj['data']
        try {
            data['updated_user'] = updateBy
            data['updated_date'] = Date.now()
            const {error, value} = validatePatient(data)
            const updateStatus = await Patient.updateOne({"_id": id}, value)
        } catch (error) {
            funcError = true
            console.log(error)
        }
        return funcError
    }
}
/* DEBUG FUNCTION */
exports.debugClear = async () => {
    await Patient.deleteMany()
}
