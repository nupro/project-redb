const mongoose = require('mongoose')
/* MODEL */
const { Food, validateFood } = require('../../models/food')
const { Foodlist, validateFoodlist } = require('../../models/foodlist')
const {FoodlistCache} = require('../../models/foodlist-cache')
/* HELPER FUNCTION */
const { makeObjId } = require('../../helper/makeObjId')
const { arangeMenu } = require('../../helper/arangeMenu')
const { forceInsertModel, insertArrayNative } = require('../../helper/forceInsertModel')

/*----- SELLECT ALL CONTROLLER-----*/
exports.fetchAll = async () => {
    const result = await Foodlist.find().sort('foodlist_id')
    return result
}

/*----- SELECT ONECE CONTROLLER -----*/
exports.fetchFirstOne = async (obj) => {
    return await Foodlist.findOne()
}

/*----- SELECT ONECE CONTROLLER -----*/
exports.fetchOnce = async (obj) => {
    return await Foodlist.findOne(obj)
}

/*----- DELETE ONCE CONTROLLER -----*/
exports.delOnce = async (obj) => {
    let funcError = false
    try {
        resultDel = await Foodlist.deleteOne(obj)
        funcError = (resultDel['n'] === 0) ? true : false
    } catch (error) {
        funcError = true
    }
    return funcError
}

/*----- CREATE CONTROLLER -----*/
/* simple model
{
    "foodlist_id": 101,
    "label": "Normal Meal",
    "menu": [],
    "routine": Date,
    "created_user": "admin",
    "updated_user": "admin"
}*/
exports.create = async (obj) => {
    const tags = obj.tag
    let textName = obj.name
    let textDetail = obj.detail
    let funcError = false
    // set simple json model
    const modelPattern = {
        'มื้อเช้า': {
          'จานหลัก': [],
          'จานรอง': []
        },
        'มื้อกลางวัน': {
          'จานหลัก': [],
          'จานรอง': []
        },
        'มื้อเย็น': {
            'จานหลัก': [],
            'จานรอง': []
        }
    }

    let bucket = await forceInsertModel(modelPattern, tags)
    await FoodlistCache.deleteMany()
    console.log('--- MAIN FUNC')
    console.log(bucket)
    let index = 0
    let trimPonter = 0 | []
    let pointerNow = '0'
    const dbPointer = await Foodlist.find({}, {'pointer': 1}).limit(1)
    if (dbPointer.length > 0){
      pointerNow = parseInt(dbPointer[0].pointer)
    }
    let simpleModel = {
        "label": tags.join().trim(),
        "name": textName.trim(),
        "detail": textDetail,
        "menu": bucket,
        "disease": tags,
        "pointer": pointerNow,
        "foodlist_id": 0,
        "created_user": "admin",
        "updated_user": "admin"
    }
    try {
        const foodlist = new Foodlist(simpleModel)
        const passObj = Foodlist
        const fieldId = 'foodlist_id'
        foodlist[fieldId] = (await makeObjId(passObj, fieldId)) + 1
        const { error, value } = validateFoodlist(obj)
        await foodlist.save()
    } catch (error) {
        funcError = true
    }
    return funcError
}

/*----- UPDATE CONTROLLER -----*/
/* simple model
{
    "id": "00000001111111222222",
    "by": "admin",
    "data": {
        "foodlist_id": 101,
        "label": "Normal Meal",
        "menu": [],
        "routine": Date,
        "created_user": "admin",
        "updated_user": "admin"
    }
}
*/

exports.update = async (obj) => {
    let funcError = false
        /* cache variable data */
        const id = obj['id']
        const updateBy = obj['by']
        const data = obj['data']
    try {
        data['updated_user'] = updateBy
        data['updated_date'] = Date.now()
        const {error, value} = validateFoodlist(data)
        const updateStatus = await Foodlist.updateOne({"_id": id}, value)
    } catch (error) {
        funcError = true
    }
    return funcError
}

/* DEBUG FUNCTION */
exports.debugClear = async () => {
    await Foodlist.deleteMany()
}

exports.updatePoniter = async (p) => {
  let funcError = false
  let dbPointer = await Foodlist.find({}, {'pointer': 1}).limit(1)
  if((p == 'p' || p == 'n') && (dbPointer.length > 0)){
    let move = (p == 'n')? +1 : -1
    move = parseInt(dbPointer[0].pointer) + move
    move = (move < 0) ? 0 : move
    move = (move > 13) ? 0 : move
    await Foodlist.updateMany({}, { pointer: move }, {multi: true})
    funcError = true
  }
  return funcError
}
