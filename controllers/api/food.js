const mongoose = require('mongoose')
/* MODEL */
const { Food, validateFood } = require('../../models/food')
/* HELPER FUNCTION */
const { makeObjId } = require('../../helper/makeObjId')

/*----- SELLECT ALL CONTROLLER-----*/
exports.fetchAll = async () => {
    return await Food.find().sort('food_id')
}
/*----- SELECT ONECE CONTROLLER -----*/
exports.fetchOnce = async (obj) => {
    return await Food.findOne(obj)
}
/*----- DELETE ONCE CONTROLLER -----*/
exports.delOnce = async (obj) => {
    let funcError = false
    try {
        resultDel = await Food.deleteOne(obj)
        funcError = (resultDel['n'] === 0) ? true : false
    } catch (error) {
        funcError = true
    }
    return funcError
}
/*----- CREATE CONTROLLER -----*/
exports.create = async (obj) => {
    let funcError = false
    try {
        const food = new Food(obj)
        const passObj = Food
        const fieldId = 'food_id'
        food[fieldId] = (await makeObjId(passObj, fieldId)) + 1
        const { error, value } = validateFood(food)
        await food.save()
    } catch (error) {
        //console.log(error)
        funcError = true
    }
    return funcError
}
/*----- UPDATE CONTROLLER -----*/
exports.update = async (obj) => {
    let funcError = false
    funcError = (!(obj.hasOwnProperty('id') && obj.hasOwnProperty('by') && obj.hasOwnProperty('data'))) ? true : false
    if(funcError == false) {
        /* cache variable data */
        const id = obj['id']
        const updateBy = obj['by']
        const data = obj['data']
        try {
            data['updated_user'] = updateBy
            data['updated_date'] = Date.now()
            const {error, value} = validateFood(data)
            const updateStatus = await Food.updateOne({"_id": id}, value)
        } catch (error) {
            funcError = true
            console.log(error)
        }
        return funcError
    }
}
/*************/
/* exports.update = async (obj) => {
    let funcError = false
    try {
        const conditionQuery = { 'food_id': obj['food_id'] }
        const { error, value } = validateFood(obj)
        console.log(value)
        const updateStatus = await Food.findOneAndUpdate(conditionQuery, value)
    } catch (error) {
        funcError = true
    }
    return funcError
} */
/* DEBUG FUNCTION */
exports.debugClear = async () => {
    await Food.deleteMany()
}
const seedJson  = require('../../seeder/food.json')
exports.debugSeed = async () => {
    for (seed of seedJson) {
      await this.createSeeder(seed)
    }
}
/* sub create */
exports.createSeeder = async (obj) => {
    try {
        const food = new Food(obj)
        await food.save()
    } catch (error) {
      console.log(' ---- Food Seeder ---- ')
      console.log(error)
    }
}
