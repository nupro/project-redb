/* Mobile Controller */

/* Database mongo connect */
const mongoose = require('mongoose')

/* MODEL */
const { User } = require('../../models/user')
const { Disease } = require('../../models/disease')
const { Food } = require('../../models/food')
const { Ingredient } = require('../../models/ingredient')

/* HELPER FUNCTION*/
/* ... */


/*----- ESAMPLE CONTROLLER-----*/
exports.test = async (data) => {
    const result = await User.find(data)
    return { "result": result[0]['password']}
}

/*----- COUNTER CONTROLLER-----*/
exports.counter = async () => {
    const result = await Math.random()
    return result
}

/* EXSAMPLE CONTROLLER
exports.<NAME FUNCTION> = async (<PARAM>) => {
    const result = await <DO SOME THING>
    return result
}
*/
