const jwt = require('jsonwebtoken')
const config = require('config')
const express = require('express')

const router = express.Router()

/* middleware */
router.use(express.static('public'))

router.get('/', (req, res) => {
    if(req.session.user) res.redirect('/')
    else res.status(200).render('pages/login')
})

router.post('/', async (req, res) => {
    try{
        const decoded = await jwt.verify(req.body.token, config.get('jwtPrivateKey'))
        req.session.jwt = req.body.token
        req.session.user = decoded
    }catch(error){
        console.log(error)
    }
    res.status(200).send('login success')
})

module.exports = router
