const patientController = require('../../controllers/api/patient')
const diseaseController = require('../../controllers/api/disease')
const _ = require('lodash')
const express = require('express')
const router = express.Router()

router.get('/', async (req, res) => {
    const result = await patientController.fetchAll()
    res.status(200).send(result)
})

router.get('/:name', async (req, res) => {
    const name = req.params.name
    const result = await patientController.fetchOnce({ 'name': name })
    res.status(200).send(result)
})
// list all patiat with group
router.get('/list/group', async (req, res) => {
    let diseaseName = await diseaseController.fetchAllOnlyName()
    let patientList = await patientController.fetchAll()
    result = [patientList.length, patientList, diseaseName]
    res.status(200).send(result)
})
// count specify disease type
router.get('/list/type', async (req, res) => {
    let patientList = await patientController.fetchAll()
    let tags = req.query.tag.split(',')
    let count = 0
    for(patient of patientList){
      let patientDisease = patient['disease'][0]
      let index = 0
      for (tag of tags){
        if(patientDisease == tag){
          count += 1
          break
        }
        index ++
      }
    }
    // console.log(count)
    res.status(200).send(count.toString())
})

router.get('/find/:group', async (req, res) => {
    const group = req.params.group
    const keyObj = { 'disease_group': group}
    const result = await patientController.find(keyObj)
    res.status(200).send(result)
})

router.post('/', async (req, res) => {
    const funcError = await patientController.create(req.body)
    if (funcError) res.status(400).send('Error to create')
    res.status(200).send('Success to create')
})

router.put('/', async (req, res) => {
    const funcError = await patientController.update(req.body)
    if (funcError) res.status(400).send('Error to update')
    res.status(200).send('Success to update')
})

router.delete('/:id', async (req, res) => {
    const id = req.params.id
    const funcError = await patientController.delOnce({ '_id': id })
    if (funcError) res.status(400).send(`Error to delete patient_id: ${id}`)
    res.status(200).send(`Success to delete patient_id: ${id}`)
})

/* DEBUG MODE */
router.delete('/debug/clear', async (req, res) => {
    await roomController.debugClear()
    res.status(200).send('All data had been clear')
})

module.exports = router
