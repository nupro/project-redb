const { User } = require('../../models/user')
const _ = require('lodash')
const Joi = require('joi')
const config = require('config')
const jwt = require('jsonwebtoken')
const express = require('express')
const router = express.Router()

/* HELPER FUNCTION */
const { hashPass } = require('../../helper/hashPass')

/*
 SIMPLE REQUEST
 {
    "email": "simple@email.com",
    "password": "itOnlyOneSerect4you"
 }
 */
router.post('/', async (req, res) => {
    try{
        const { error } = validateAuthen(req.body)
        console.log(error)
        if (error) return res.status(400).send('Authentication has fail\n' + error.details[0].message)

        let user = await User.findOne({ email: req.body.email })
        if (!user) return res.status(400).send('Invalid email or password')
        if (hashPass(req.body.password) != user.password) res.status(400).send('Invalid email or password')

        /* set jwt */
        const userInfo = {
            _id: user._id,
            email: user.email,
            type: user.type
        }
        const privateKey = config.get('jwtPrivateKey')
        const token = jwt.sign(userInfo, privateKey)

        res.status(200).send(token)

    } catch(error){
        console.log(error)
    }

})

/* validate login request*/
function validateAuthen(req){
    const schema = {
        email: Joi.string().min(5).max(60).required().email(),
        password: Joi.string().min(4).required()
    }
    return Joi.validate(req, schema)
}

module.exports = router
