const diseaseController = require('../../controllers/api/disease')
const authen = require('../../middleware/api/authen')
const _ = require('lodash')
const express = require('express')
const router = express.Router()

router.get('/', async (req, res) => {
    const result = await diseaseController.fetchAll()
    res.status(200).send(result)
})

router.get('/:name', async (req, res) => {
    const name = req.params.name
    // const result = await diseaseController.fetchOnce({$or: [{'name_en': name}, {'name_th': name}]})
    // res.status(200).send(name)
    const result = await diseaseController.fetchOnce({'name_en': name})
    res.status(200).send(result)
})

router.post('/', async (req, res) => {
    const funcError = await diseaseController.create(req.body)
    if (funcError) res.status(400).send('Error to create')
    res.status(200).send('Success to create')
})

router.put('/', async (req, res) => {
    const funcError = await diseaseController.update(req.body)
    if (funcError) res.status(400).send('Error to update')
    res.status(200).send('Success to update')
})

router.delete('/:id', async (req, res) => {
    const id = req.params.id
    const funcError = await diseaseController.delOnce({ '_id': id })
    if (funcError) res.status(400).send(`Error to delete disease_id: ${id}`)
    res.status(200).send(`Success to delete disease_id: ${id}`)
})
/* DEBUG MODE */
router.get('/debug/clear', async (req, res) => {
    await diseaseController.debugClear()
    res.status(200).send('All data had been clear')
})

router.get('/debug/seed', async (req, res) => {
    await diseaseController.debugSeed()
    res.status(200).send('Data had seeded')
})

module.exports = router
