const userController = require('../../controllers/api/user')
const _ = require('lodash')
const express = require('express')
const router = express.Router()

router.get('/', async (req, res) => {
    const result = await userController.fetchAll()
    res.status(200).send(result)
})

router.get('/:email', async (req, res) => {
    const email = req.params.email
    const result = await userController.fetchOnce({'email': email})
    res.status(200).send(result)
})

router.post('/', async (req, res) => {
    const funcError = await userController.create(req.body)
    if (funcError) res.status(400).send('Error to create')
    res.status(200).send('Success to create')
})

router.put('/', async (req, res) => {
    const funcError = await userController.update(req.body)
    if (funcError) res.status(400).send('Error to update')
    res.status(200).send('Success to update')
})

router.delete('/:id', async (req, res) => {
    const id = req.params.id
    const funcError = await userController.delOnce({'_id': id})
    if (funcError) res.status(400).send(`Error to delete user_id: ${id}`)
    res.status(200).send(`Success to delete user_id: ${id}`)
})

/* DEBUG MODE */
router.get('/debug/clear', async (req, res) => {
    await userController.debugClear()
    res.status(200).send('All data had been clear')
})

router.get('/debug/seed', async (req, res) => {
    await userController.debugSeed()
    res.status(200).send('Data had seeded')
})

module.exports = router
