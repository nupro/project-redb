const foodlistController = require('../../controllers/api/foodlist')
const patientController = require('../../controllers/api/patient')
const _ = require('lodash')
const express = require('express')
const router = express.Router()

router.get('/', async (req, res) => {
    const result = await foodlistController.fetchAll()
    res.status(200).send(result)
})


router.get('/at/first', async (req, res) => {
    const result = await foodlistController.fetchFirstOne()
    res.status(200).send(result)
})

router.get('/export/order', async (req, res) => {
    const menus = await foodlistController.fetchFirstOne()
    const patients = await patientController.fetchAll()
    const tagDisease = menus['disease']
    let consumer = 0
    // find disease on patients list
    for(patient of patients){
      for(disease of tagDisease){
        if(disease == patient['disease'][0]){
          consumer ++
          break
        }
      }
    }
  result = [
    consumer.toString(),
    menus
  ]
  res.status(200).send(result)
})

router.get('/:label', async (req, res) => {
    const label = req.params.label
    const result = await foodlistController.fetchOnce({ 'label': label })
    res.status(200).send(result)
})

router.post('/', async (req, res) => {
    if(!(!req.body.tag)){
        if(req.body.tag.length > 0){
            /* controller here */
            const funcError = await foodlistController.create(req.body)
            if (funcError) res.status(400).send('Error to create')
            else res.status(400).send('Success Create')
        }else{
            res.send('novalue')
        }
    }else{
        res.send('nodata')
    }
})

router.put('/', async (req, res) => {
    const funcError = await foodlistController.update(req.body)
    if (funcError) res.status(400).send('Error to update')
    res.status(200).send('Success to update')
})
router.put('/p', async (req, res) => {
    if(req.body.p){
      const funcError = await foodlistController.updatePoniter(req.body.p)
      if (funcError) res.status(200).send('pointer had update')
    }else{
      res.status(400).send('data not found')
    }
})

router.delete('/:id', async (req, res) => {
    const id = req.params.id
    const funcError = await foodlistController.delOnce({ '_id': id })
    if (funcError) res.status(400).send(`Error to delete foodlist_id: ${id}`)
    res.status(200).send(`Success to delete foodlist_id: ${id}`)
})


module.exports = router
