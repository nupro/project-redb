const roomController = require('../../controllers/api/room')
const _ = require('lodash')
const express = require('express')
const router = express.Router()

router.get('/', async (req, res) => {
    const result = await roomController.fetchAll()
    res.status(200).send(result)
})

router.get('/:id', async (req, res) => {
    const roomId = req.params.id
    const result = await roomController.fetchOnce({ 'room_id': roomId })
    res.status(200).send(result)
})

router.post('/', async (req, res) => {
    const funcError = await roomController.create(req.body)
    if (funcError) res.status(400).send('Error to create')
    res.status(200).send('Success to create')
})

router.put('/', async (req, res) => {
    const funcError = await roomController.update(req.body)
    if (funcError) res.status(400).send('Error to update')
    res.status(200).send('Success to update')
})

router.delete('/:id', async (req, res) => {
    const id = req.params.id
    const funcError = await roomController.delOnce({ '_id': id })
    if (funcError) res.status(400).send(`Error to delete _id: ${id}`)
    res.status(200).send(`Success to delete _id: ${id}`)
})

/* DEBUG MODE */
router.delete('/debug/clear', async (req, res) => {
    await roomController.debugClear()
    res.status(200).send('All data had been clear')
})

module.exports = router
