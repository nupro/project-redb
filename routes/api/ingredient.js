const ingredientController = require('../../controllers/api/ingredient')
const _ = require('lodash')
const express = require('express')
const router = express.Router()

router.get('/', async (req, res) => {
    const result = await ingredientController.fetchAll()
    res.status(200).send(result)
})

router.get('/:name', async (req, res) => {
    const id = req.params.name
    const result = await ingredientController.fetchOnce({ 'name_en': name })
    res.status(200).send(result)
})

router.post('/', async (req, res) => {
    const funcError = await ingredientController.create(req.body)
    if (funcError) res.status(400).send('Error to create')
    res.status(200).send('Success to create')
})

router.put('/', async (req, res) => {
    const funcError = await ingredientController.update(req.body)
    if (funcError) res.status(400).send('Error to update')
    res.status(200).send('Success to update')
})

router.delete('/:id', async (req, res) => {
    const id = req.params.id
    const funcError = await ingredientController.delOnce({ '_id': id })
    if (funcError) res.status(400).send(`Error to delete ingredient_id: ${id}`)
    res.status(200).send(`Success to delete ingredient_id: ${id}`)
})

/* DEBUG MODE */
router.get('/debug/clear', async (req, res) => {
    await ingredientController.debugClear()
    res.status(200).send('All data had been clear')
})

router.get('/debug/seed', async (req, res) => {
    await ingredientController.debugSeed()
    res.status(200).send('Data had seeded')
})

module.exports = router
