/* Mobile API entry point */
const mobileController = require('../../controllers/api/mobile')
const _ = require('lodash')
const express = require('express')
const router = express.Router()

router.get('/', async (req, res) => {
    const result = await mobileController.counter()
    res.status(200).send(result)
})


/* Esample router */
router.post('/simple', async (req, res) => {
    const obj = req.body
    const result = await mobileController.test(obj)
    res.status(200).send(JSON.stringify(result))
})

/* ----- EXSAMPLE ROUTER -----
 URL:data
 req.params.data
 req.body
 ---------------------
router.get(<URL>, async (req, res) => {
    const result = await <DO SOME THING>
    res.status(200).send(result)
})
*/

module.exports = router

