const foodController = require('../../controllers/api/food')
const _ = require('lodash')
const express = require('express')
const router = express.Router()

router.get('/', async (req, res) => {
    const result = await foodController.fetchAll()
    res.status(200).send(result)
})

router.get('/:name', async (req, res) => {
    const name = req.params.name
    const result = await foodController.fetchOnce({ 'name_en': name })
    res.status(200).send(result)
})

router.post('/', async (req, res) => {
    const funcError = await foodController.create(req.body)
    if (funcError) res.status(400).send('Error to create')
    res.status(200).send('Success to create')
})

router.put('/', async (req, res) => {
    const funcError = await foodController.update(req.body)
    if (funcError) res.status(400).send('Error to update')
    res.status(200).send('Success to update')
})

router.delete('/:id', async (req, res) => {
    const id = req.params.id
    const funcError = await foodController.delOnce({ '_id': id })
    if (funcError) res.status(400).send(`Error to delete food_id: ${id}`)
    res.status(200).send(`Success to delete food_id: ${id}`)
})

/* DEBUG MODE */
router.get('/debug/clear', async (req, res) => {
    await foodController.debugClear()
    res.status(200).send('All data had been clear')
    // res.status(200).send('All data clear disable')
})

router.get('/debug/seed', async (req, res) => {
    await foodController.debugSeed()
    res.status(200).send('Data had seeded')
})
module.exports = router
