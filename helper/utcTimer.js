exports.utcReader = (utc) => {
    return new Date.parse(utc)
}
exports.utcNow = () => {
    const now = new Date
    return Date.UTC(now.getUTCFullYear(),now.getUTCMonth(), now.getUTCDate(), now.getUTCHours(), now.getUTCMinutes(), now.getUTCSeconds(), now.getUTCMilliseconds())
}
