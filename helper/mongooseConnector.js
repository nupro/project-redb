const mongoose = require('mongoose')

exports.mongooseConnector = () => {
    mongoose.connect('mongodb://mongo/projectredb')
        .then(() => console.log('Connected to MongoDB...'))
        .catch(err => {
            console.error('Could not connect to MongoDB...')
            mongooseConnector()
        })
}
