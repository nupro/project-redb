/* HELPER FUNCTION*/
const mongoose = require('mongoose')
/* MODEL */
const {Food} = require('../models/food')
const {FoodlistCache} = require('../models/foodlist-cache')

exports.forceInsertModel = async (pattern, tags) => {
    const isCache = await storageCache(pattern, tags)
    if(isCache){
        const result = await FoodlistCache.find({},{_id: 0, __v: 0}).limit(14)
        return result
    }else{
        return []
    }
}

async function storageCache(pattern, tags){
    let result = true
    for(let index = 0; index < 14; index++){
        try{
            let tmp = await insertArray(index, tags, pattern)
            const fcache = new FoodlistCache(tmp)
            await fcache.save()
        }catch(err){
            console.log(`Error insert foodlistCache : ${err}`)
            result = false
        }
    }
    return result
}

async function insertArray(index, tags, dummy){
    for (repastFood of ['มื้อเช้า', 'มื้อกลางวัน', 'มื้อเย็น']) {
        for (typeFood of ['จานหลัก', 'จานรอง']) {
            dummy[repastFood][typeFood] = await Food.find({"ban": {$nin: tags}, "repast": repastFood, "type": typeFood}).limit(1).skip(index)
            //dummy[repastFood][typeFood] = Math.floor(Math.random() * Math.floor(300))
        }
    }
    return dummy
}

