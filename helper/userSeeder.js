/* HELPER FUNCTION*/
// const {hashPass} = require('./hashPass')
// const {utcNow} = require('./utcTimer')

/* USER INFO */
const creator = 'system'
// admin default
const adminPass = '1234a'
const adminImage = 'no-image'
const adminType = 'admin'
const adminEmail = 'admin@email.com'

exports.userSeeder = () => {
    return [{
        "email": adminEmail,
        "password": adminPass,
        "type": adminType,
        "image": adminImage,
        "created_user": creator,
        "updated_user": creator
      }]
}
