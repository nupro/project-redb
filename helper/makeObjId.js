const mongoose = require('mongoose')

mongoose.connect('mongodb://mongo/projectredb')
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDB...'))

exports.makeObjId = async (obj, field) => {
    const Collection = obj
    let lastestId = await Collection.findOne({}, {[field]: 1}).sort({[field]:-1})
    lastestId = (lastestId === null) ? 0 : lastestId[field]
    return lastestId
}
