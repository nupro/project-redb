new Vue({
    el: '#vue-app-patient',
    data: {
        enNameCreate: '',
        thNameCreate: '',
        diseaseCreate: [],
        createBy: currentUser,
    },
    methods:{
        addList: async function(){
            /* reload selection */
            helperSelectPicker('selectpicker-disease', 'disease', 'name_en', 'name_th')
            /* set post request structure*/
            const checkEngAndNoBlank = (this.enNameCreate != '' && heplerFilterOnlyEngStr(this.enNameCreate))
            const checkThAndNoBlank = (this.thNameCreate != '' && !heplerFilterOnlyEngStr(this.thNameCreate))
            if(checkEngAndNoBlank && checkThAndNoBlank){
                let data = {
                  name_th: this.thNameCreate.toLowerCase(),
                  name_en: this.enNameCreate.toLowerCase(),
                  disease: this.diseaseCreate,
                  // disease_group: this.diseaseCreate.join('-'),
                  disease_group: this.diseaseCreate,
                  created_user: this.createBy,
                  updated_user: this.createBy
                }
                helperPostLists(data, hostAPI)
                /* clear cache vue var model */
                const messageNotiSussess = 'Add new '+ entryPoint + ' ' + this.enNameCreate + ' (' + this.thNameCreate + ')'
                helperNoti($('#noti'), 'success', messageNotiSussess)
                /* clear v-model */
                this.thNameCreate = ''
                this.enNameCreate = ''
                await tmpCreateList()
            }else{
                const messageNotiFail = 'Fail to add new ' + entryPoint + ' to list !!! '
                helperNoti($('#noti'), 'danger', messageNotiFail)
            }
        }
    },
    beforeMount: function(){
        tmpCreateList()
        helperSelectPicker('selectpicker-disease', 'disease', 'name_en', 'name_th')
    },
    mounted: function(){
        console.log('Mounted')
    },
})
