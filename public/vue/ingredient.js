new Vue({
    el: '#vue-app-ingredient',
    data: {
        enNameCreate: '',
        thNameCreate: '',
        thUnitCreate: '',
        priceCreate: '',
        createBy: currentUser,
    },
    methods:{
        addList: async function(){
            /* set post request structure*/
            const checkEngAndNoBlank = (this.enNameCreate != '' && heplerFilterOnlyEngStr(this.enNameCreate))
            const checkThAndNoBlank = (this.thNameCreate != '' && !heplerFilterOnlyEngStr(this.thNameCreate))
            if(checkEngAndNoBlank && checkThAndNoBlank){
                let data = {
                    name_th: this.thNameCreate,
                    name_en: this.enNameCreate.toLowerCase(),
                    name_unit: this.thUnitCreate,
                    price: this.priceCreate,
                    created_user: this.createBy,
                    updated_user: this.createBy
                }
                helperPostLists(data, hostAPI)
                /* clear cache vue var model */
                const messageNotiSussess = 'Add new '+ entryPoint + ' ' + this.enNameCreate + ' (' + this.thNameCreate + ')'
                helperNoti($('#noti'), 'success', messageNotiSussess)
                /* clear v-model */
                this.thNameCreate = ''
                this.enNameCreate = ''
                this.thUnitCreate = ''
                this.priceCreate = ''
                await tmpCreateList()
            }else{
                const messageNotiFail = 'Fail to add new ' + entryPoint + ' to list !!! '
                helperNoti($('#noti'), 'danger', messageNotiFail)
            }
        }
    },
    beforeMount: async function(){
      await tmpCreateList()
    },
    mounted: function(){
        console.log('Mounted')
    },
})

/* set tmp function*/
async function tmpCreateList(){
  /* create table row */
  await axios(hostAPI, 'get', {'Content-Type': 'application/json; charset=utf-8'}, {})
  .then(function(res){
    let key = ['name_en', 'name_th', 'price', 'name_unit', 'created_user', 'updated_user', 'created_date', 'updated_date']    // set 2
    helperCreateLists($('#tdList'), key, res.data, hostAPI)
  })
  .catch(function(err){console.log(err)})
}
