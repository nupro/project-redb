let tmpUnitFormCreate = []
new Vue({
    el: '#vue-app-food',
    data: {
        enNameCreate: '',
        thNameCreate: '',
        diseaseAllowCreate: '',
        ingredientCreate: '',
        createBy: currentUser,
    },
    methods:{
        addList: async function(){
            this.unitFormInsert(this.ingredientCreate.length)
            ingreFormat = this.makeIngreFormat(this.ingredientCreate, tmpUnitFormCreate)
            helperSelectPicker('selectpicker-disease', 'disease', 'name_en', 'name_th')
            helperMultiSelectPicker('selectpicker-ingre', 'ingredient', 'name_th', 'name_unit', 'price', 'name_th')
            /* set post request structure*/
            //const checkEngAndNoBlank = (this.enNameCreate != '' && heplerFilterOnlyEngStr(this.enNameCreate))
            //const checkThAndNoBlank = (this.thNameCreate != '' && !heplerFilterOnlyEngStr(this.thNameCreate))
            const checkThAndNoBlank = this.thNameCreate != ''
            //if(checkEngAndNoBlank && checkThAndNoBlank){
            if(checkThAndNoBlank){
                let data = {
                    name_th: this.thNameCreate.toLowerCase(),
                    name_en: this.enNameCreate.toLowerCase(),
                    type: $('#typeCreate').val(),
                    repast: $('#repastCreate').val(),
                    ban: this.diseaseAllowCreate,
                    ingredient: ingreFormat,
                    created_user: this.createBy,
                    updated_user: this.createBy
                }
                helperPostLists(data, hostAPI)
                /* clear cache vue var model */
                const messageNotiSussess = 'Add new '+ entryPoint + ' ' + this.enNameCreate + ' (' + this.thNameCreate + ')'
                helperNoti($('#noti'), 'success', messageNotiSussess)
                /* clear v-model */
                this.thNameCreate = ''
                this.enNameCreate = ''
                this.diseaseAllowCreate = ''
                this.ingredientCreate = ''
                tmpUnitFormCreate = []
                this.typeCreate = 'main'
                this.repastCreate = 'breakfast'
                await tmpCreateList()
                $('#ingreDetail').html('')
            }else{
                const messageNotiFail = 'Fail to add new ' + entryPoint + ' to list !!! '
                helperNoti($('#noti'), 'danger', messageNotiFail)
            }
        },
        makeIngreFormat: function(ingreForm, unitForm){
            let limit = ingreForm.length
            let result = [...Array(limit)]
            for (i=0; i < limit; i++){
                item = ingreForm[i].split('-')
                unit = unitForm[i]
                price = item[2]
                result[i] = [item[0], unit, item[1], price]
            }
            return result
        },
        unitFormInsert: function(length){
            for (i = 0; i < length; i++) {
                value = $('#inputUnit_'+i).val()
                tmpUnitFormCreate[i] = value
            }
        },
        unitForm: function(){
            htmlFormInput = ""
            index = 0
            tmpUnitFormCreate = [...Array(this.ingredientCreate.length)]
            for ( ingreUint of this.ingredientCreate ) {
                item = ingreUint.split('-')
                htmlFormInputRow = '<tr>' +
                    '<td><span>' + item[0] + '</span></td>' +
                    '<td style="width:300px;"><div className="input-group">' +
                    '<input type="text" id="inputUnit_'+index+'" style="width:5px;" className="form-control">' +
                    '<div className="input-group-prepend">' +
                    '<span class="input-group-text" id="inputUnit">' + item[1] + '&nbsp;<b>/</b>&nbsp;' +item[2] + '&nbsp;บาท</span>' +
                    '</div></div></td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr>'
                htmlFormInput = htmlFormInput + htmlFormInputRow
                index += 1
            }
            htmlFormUint = '<table>' + htmlFormInput + '</table>'
            $('#buttonCreate').css("visibility", "visible")
            $('#ingreDetail').html(htmlFormUint)
            $('[className="input-group"]').addClass('input-group')
            $('[className="input-group-prepend"]').addClass('input-group-prepend')
            $('[className="input-group-text"]').addClass('input-group-text')
            $('[className="form-control"]').addClass('form-control')
        }
    },
    beforeMount: function(){
        tmpCreateList()
        helperSelectPicker('selectpicker-disease', 'disease', 'name_en', 'name_th')
        helperMultiSelectPicker('selectpicker-ingre', 'ingredient', 'name_th', 'name_unit', 'price', 'name_th')
    },
    mounted: function(){
        console.log('Mounted')
    },
})
