/* Export Order */
ingreStack = []
function helperCreateExportOrder(el, items, consumerCount){
    let consumer = parseInt(consumerCount)
      for (menu of items.menu){
            for (repast of Object.values(menu)){
                for (repastType of Object.values(repast)){
                      for(ingre of Object.values(repastType[0]['ingredient'])){
                          keep([
                            ingre[0],
                            Number(ingre[1]),
                            ingre[2],
                            Number(ingre[3])
                          ])
                      }// end for
                }
            }
        }
    htmlArrangement(el, ingreStack, consumer)
}

function htmlArrangement(el, lists, consumer){
    index = 1
    tbTitle = '<h5>สำหรับผู้ป่วยจำนวน '+ consumer +'คน ใน 14วัน</h5>'
    tbOp = '<table border="1">'
    tbCl = '</table>'
    th = '<tr>' +
            '<td>#</td>' +
            '<td>วัตถุดิบ</td>' +
            '<td>ปริมาณ</td>' +
            '<td>หน่วย</td>' +
            '<td>ราคา/หน่วย</td>' +
            '<td>ราคารวม/บาท</td>' +
        '</tr>'
    td = ''
    allPrice = 0
    for(list of lists){
        let name = list[0]
        let volumn = (list[1] * consumer) // with consumer
        //let volumn = list[1]
        let pricePerUnit = list[3]
        let unit = list[2]
        let price = (volumn * pricePerUnit)
        allPrice += price
        td = td + '<tr>' +
            '<td>' + index + '</td>' +
            '<td>' + name  + '</td>' +
            '<td>' + numeral(volumn).format('0,0.00')+ '</td>' +
            '<td>' + unit + '</td>' +
            '<td>' + numeral(pricePerUnit).format('0,0.00') + '</td>' +
            '<td>' + numeral(price).format('0,0.00')   + '</td>' +
        '</tr>'
        index ++
    }
    tdSum = '<tr>' +
            '<td>&nbsp;</td>' +
            '<td>&nbsp;</td>' +
            '<td>&nbsp;</td>' +
            '<td>&nbsp;</td>' +
            '<td><span>รวม</span></td>' +
            '<td>' + numeral(allPrice).format('0,0.00') + '</td>' +
        '</tr>'
    el.html(tbTitle + tbOp + th + td + tdSum + tbCl)
}

function keep(item, callback){
    name = item[0]
    unitName = item[2]
    unit = item[1]/80
    // unit = item[1]
    price = item[3]
    index = 0
    found = false
    for (itemStack of ingreStack){
        if(itemStack[0] == name){
            ingreStack[index][1] += unit
            found = true
            break
        }
        index ++
    }
    if(found == false){
        ingreStack.push([name, unit, unitName, price])
    }
}
