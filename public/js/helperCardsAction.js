/* helper custom Create item card */
function helperCreateCard(el, items, apiURL, userType){
  let htmlResult = ''
  for(let i = 0; i < items.length; i++){
    let datePointer = parseInt(items[i]['pointer']) + 1
    let colorPointer = '#117A65'
    colorPointer = (datePointer > 3 && datePointer < 7) ? '#27AE60' : colorPointer
    colorPointer = (datePointer > 6 && datePointer < 10) ? '#D4AC0D' : colorPointer
    colorPointer = (datePointer > 9 && datePointer < 13) ? '#E67E22' : colorPointer
    colorPointer = (datePointer > 12) ? '#E74C3C' : colorPointer
    let headDiv = ''
    let cardNumber = ''
    let cardDate = ''
    let cardLabel = ''
    if(i == 0){
      headDiv = '<div className="card" style="margin:5px;min-width:300px;"><div className="card-body">'
      cardNumber = '<h1 onclick="goListPage(\'export-menu\')" className="card-subtitle mb-2 text-muted" style="float: left;">' + (i+1) + '&nbsp;</h1>'
      cardDate = '<h6 className="card-subtitle mb-2 text-muted" style="float:right;">วันที่ : <b><span style="color:'+ colorPointer +';">' + datePointer + '</span></b>/14</h6>'
      cardLabel = '<h5 onclick="goListPage(\'export-menu\')" className="card-title">' + items[i]['name'] + '</h5>'

    } else {
      headDiv = '<div className="card" style="cursor:not-allowed;background:#CDCDCD;margin:5px;min-width:300px;"><div className="card-body">'
      cardNumber = '<h1 className="card-subtitle mb-2 text-muted" style="float: left;">' + (i+1) + '&nbsp;</h1>'
      cardDate = '<h6 className="card-subtitle mb-2 text-muted" style="float:right;">วันที่ : <b><span>--</span></b>/14</h6>'
      cardLabel = '<h5 className="card-title" style="color:#151515;">' + items[i]['name'] + '</h5>'
    }
    //let cardTag = '<h6 className="card-subtitle mb-2 text-muted"><span className="tagsBg">Tags</span> ' + items[i]['label'] + '</h6>'
    let cardTag = '<span>&nbsp;</span>'
    let cardText = '<hr><div className="card-text">' + items[i]['detail'] + '</div>'
    let cardOpenLink = ''
    let tailDiv = '<a className="card-link" style="color:red;float:right;" href="#del" onclick="helperDelCards(\'' + items[i]._id + '\', \'' + apiURL+ '\')"><span className="fa fa-trash-o"></span></a></div></div>'
    tailDiv = (userType == 'admin' || userType == 'nutritionist') ? tailDiv : ''
    htmlResult = htmlResult + headDiv + cardNumber + cardDate + cardLabel + cardTag + cardText + cardOpenLink + tailDiv
  }
  el.html(htmlResult)
  // add class css
  $('[className="card"]').addClass('card')
  $('[className="card-body"]').addClass('card-body')
  $('[className="card-subtitle mb-2 text-muted"]').addClass('card-subtitle mb-2 text-muted')
  $('[className="card-link"]').addClass('card-link')
  $('[className="fa fa-trash-o"]').addClass('fa fa-trash-o')
  $('[className="card-subtitle mb-2 text-muted"]').addClass('card-subtitle mb-2 text-muted')
  $('[className="card-title"]').addClass('card-title')
  $('[className="card"]').addClass('card')
  $('[className="card"]').addClass('card')
  $('[className="card"]').addClass('card')
  $('[className="tagsBg"]').addClass('badge badge-dark')
}

/* helper Delete item from lists */
function helperDelCards(_id, apiURL){
    let isConfirm = confirm('ยืนยันการลบรายการอาหาร')
    if(isConfirm){
        /* config request info */
        const _url = apiURL + _id
        const _headers = {'Content-Type': 'application/json; charset=utf-8'}
        /* send request */
        axios.delete(_url, {id: _id})
        .then(function(res){
            console.log(res.data)
            tmpCreateList()
        })
        .catch(function(err){console.log(err)})
        setTimeout(function(){
          const message = "&nbsp;ไม่มีข้อมูล"
          if ($('#showCase').children().length < 1) $('#showCase').html('<center><div><h5 style="color:#495057;">' + message + '</h5><div></center>')
        },100)
    }
}

/* helper cards chage Pointer */
function helperPonterChanger(i, apiURL){
  /* config request info */
  const _url = apiURL+'p'
  const _headers = {'Content-Type': 'application/json; charset=utf-8'}
  /* send request */
  axios({
    method: 'put',
    url: _url,
    data: {p: i}
  })
  .then(function(res){
      tmpCreateList()
  })
  .catch(function(err){console.log(err)})
}
