// convert kg to g
function kgToGram(unitKg){
  unitPerGram = 0.0010000
  return unitKg/unitPerGram
}
// fillter specify unit
function specifyUnitKgToGram(volumn, unit){
  unitKg = ['กก.','กิโลกรัม','kg.','kilogram']
  if(unitKg.includes(unit)){
    return [
      kgToGram(volumn).toFixed(2),
      'กรัม'
    ]
  } else {
    return [volumn, unit]
  }
}
