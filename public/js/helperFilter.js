function heplerFilterOnlyEngStr(str){
  return (str.match(/^\s*[a-zA-Z][a-zA-Z ]*$/) != null) ? true : false
}

function heplerFilterEmailStr(str){
  return (str.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/g) != null) ? true : false
}
