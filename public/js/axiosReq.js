function axiosReq(_url, _method, _headers, _data, callback){
    axios({
        url: _url,
        method: _method,
        headers: _headers,
        data: _data
    })
    .then(function(res){
        if(callback) callback(res.data)
        console.log(res.data)
    })
    .catch(function(err){
        console.log(err)
    })
}
