/* ------------ */
/* EXPORT MENU  */
/* ------------ */
/* count consumer  */

/* count consumer  */
function helperLoadAllDisease(){
  _load_url = getHostAPI('patient')
  _load_url = _load_url + 'list/group'
  return axios(_load_url, 'get', {'Content-Type': 'application/json; charset=utf-8'}, {})
  .then(function(res){
    let diseaseList = []
    for(disease of res.data[2]){
      diseaseList.push([
        disease['name_en'],
        disease['name_th'],
        0
      ])
    }
    return [res.data[0], res.data[1], diseaseList]
  })
  .then(function(res){
    let total = res[0]
    let patients = res[1]
    let groupDisease = res[2]
    for(patient of patients){
      let patientDisease = patient['disease'][0]
      let index = 0
      for (disease of groupDisease){
        if(patientDisease == disease[0]){
          groupDisease[index][2] += 1
          break
        }
        index ++
      }
    }// end loop count disease type
    return [total, groupDisease]
  })
}
/* helper create disease group count */
function helperCreateDiseaseGroupTitle(){
  helperLoadAllDisease().then((res) => {
    let title = '<h3><span class="fa fa-bed" aria-hidden="true"></span>&nbsp;ผู้ป่วยในระบบ&nbsp;(' + res[0]+')&nbsp;คน</h3>'
    let detail = ''
    for(disease of res[1]){
      detail = detail +
      '<span>' + disease[1] + '</span>&nbsp;' +
      '<span>(' + disease[2] + ')</span>&emsp;'
    }
    diseaseListHtml = title + detail
    $('#showDiseaseList').html(diseaseListHtml)
  })
}
/* helper extract menu list */
function helperMenuExtractor(obj, pointer, consumer){
  const menuList = obj[pointer]
  /* this obj use for text translate and map key in array menu list*/
  const tranRepast = {
    'มื้อเช้า': 'มื้อเช้า',
    'มื้อกลางวัน': 'มื้อกลางวัน',
    'มื้อเย็น': 'มื้อเย็น'
  }
  const tranType = {
    'จานหลัก': 'จานหลัก',
    'จานรอง': 'จานรอง'
  }
  let htmlBox = ''
  for (repastM of Object.keys(tranRepast)) {
    const headRepastHtml = '<div className="col-lg-4">'
    const tailRepastHtml = '</div>'
    const menutextRepast = '<h4>' + tranRepast[repastM] + '</h4>'
    let detail = ''
    //console.log(tranRepast[repastM])
    //console.log(Object.keys(tranRepast[repastM]))
    for (typeM of Object.keys(tranType)) {
      const menu = menuList[repastM][typeM]
      //console.log(menu)
      const menutextRepast = '<u><h5>' + tranType[typeM] + '</h5></u>'
      const tableHead = '<table><tr>'
      const tableTail = '</tr></table>'
      const tableSubHead = '<td><table>'
      const tableSubTail = '</table></td>'
      const menuNameTh = '<tr><td>&nbsp;' + menu[0].name_th + '</td></tr>'
      const menuNameEn = '<tr><td>&nbsp;' + menu[0].name_en + '</td></tr>'
      const headMenuIngre = '<tr><td><b>วัตถุดิบ</b>'
      let menuIngre = processArrayIgre(menu[0].ingredient, consumer)
      const tailMenuIngre = '</tr></td>'
      let subTableDetail = tableSubHead + menuNameTh + menuNameEn + headMenuIngre + menuIngre + tailMenuIngre + tableSubTail
      detail = detail + menutextRepast + tableHead + subTableDetail + tableTail + '<hr>'
      //detail = ''
    }// end for type
    htmlBox = htmlBox + headRepastHtml + menutextRepast + detail + tailRepastHtml
  }// end for repast
  return htmlBox
}
/* async process array ingredient */
function processArrayIgre(arrayIngre, consumer) {
    let tmp = ''
    for (ingreItem of arrayIngre){
        // change unit kg to gram only kg unit
        let ingreNew = specifyUnitKgToGram(ingreItem[1], ingreItem[2])
        consumer = (consumer == 0)? 1 : consumer
        let name = ingreItem[0]
        // let volumn = ingreNew[0] * consumer
        let volumn = ingreNew[0]
        let unit = ingreNew[1]

        tmp = tmp + '<tr>' +
            '<td>' + name + '</td>' +
            '<td>&emsp;</td>' +
            '<td>' + volumn + '</td>' +
            '<td>&emsp;</td>' +
            '<td>' + unit + '</td>' +
            '<td>&nbsp;</td>' +
            '</tr>'
    }
    return '<table>' + tmp + '</table>'
}

/* helper custom Create item card */
function helperCreateExportDoc(el, items ,apiURL){
  let htmlResult = ''
  // for(let i = 0; i < items.length; i++){
  for(let i = 0; i < 1; i++){ // only first index
    // start extract menu
    // let group_disease = items[i]['label'].split(',').join('-')
    // count consumer for this menu //
    // const consumer = res.data.length
    const tagQuery = items[i]['label']
    let _load_url = getHostAPI('patient')
    _load_url = _load_url + 'list/type?tag=' + tagQuery
    axios(_load_url, 'get', {'Content-Type': 'application/json; charset=utf-8'}, {})
    .then(function(res){
    const consumer = parseInt(res.data)
    console.log(consumer)
    // no count
    const detailMenu = helperMenuExtractor(items[i]['menu'], items[i]['pointer'], consumer)
    // end extract menu
    let datePointer = parseInt(items[i]['pointer']) + 1
    let colorPointer = '#27AE60'
    colorPointer = (datePointer > 3 && datePointer < 7) ? '#2ECC71' : colorPointer
    colorPointer = (datePointer > 6 && datePointer < 10) ? '#D4AC0D' : colorPointer
    colorPointer = (datePointer > 9 && datePointer < 13) ? '#E67E22' : colorPointer
    colorPointer = (datePointer > 12) ? '#E74C3C' : colorPointer
    let headDiv = '<div className="card" style="margin:5px;min-width:300px;"><div className="card-body">'
    let cardNumber = '<h1 className="card-subtitle mb-2 text-muted" style="float: left;">*&nbsp;</h1>'
    let cardDate = '<h6 className="card-subtitle mb-2 text-muted" style="float:right;">วันที่ : <b><span style="color:'+ colorPointer +';">' + datePointer + '</span></b>/14</h6>'
    let cardLabel = '<h5 className="card-title">' + items[i]['name'] + '&nbsp;(' + consumer + ')</h5>'
    // let cardTag = '<h6 className="card-subtitle mb-2 text-muted"><b>รายชื่อโรค</b> : ' + items[i]['label'] + '</h6><hr>'
    let cardTag = '<hr>'
    let cardText = '<div className="row">' + detailMenu + '</div>'
    let tailDiv = '</div></div>'
    htmlResult = htmlResult + headDiv + cardNumber + cardDate + cardLabel + cardTag + cardText + tailDiv
    el.html(htmlResult)
  // add class css
  $('[className="card"]').addClass('card')
  $('[className="card-body"]').addClass('card-body')
  $('[className="card-subtitle mb-2 text-muted"]').addClass('card-subtitle mb-2 text-muted')
  $('[className="card-link"]').addClass('card-link')
  $('[className="card-subtitle mb-2 text-muted"]').addClass('card-subtitle mb-2 text-muted')
  $('[className="card-title"]').addClass('card-title')
  $('[className="row"]').addClass('row')
  $('[className="col-lg-4"]').addClass('col-lg-4')
  $('[className="imgMenu"]').addClass('imgMenu')
  })
  }
}
