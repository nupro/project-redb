/* helpe generate select form option */

//params [html element] [api host] [select value] [select option]
function helperSelectPicker(elId, api, fieldValue, fieldOption){
  //config request detail
  _load_selection_url = getHostAPI(api)
  _load_selection_headers = {'Content-Type': 'application/json; charset=utf-8'}
  // send get request
  _load_selection_response = axios.get(_load_selection_url, _load_selection_headers)
  // create options for select form
  _load_selection_response.then(function(res){
    let txtHTML = ''
    for(i=0; i < res.data.length; i++){
      txtHTML = txtHTML + '<option value="' + res.data[i][fieldValue] + '">' + res.data[i][fieldOption] + '</option>'
    }
  // add response content to html element
    $('#' + elId).html(txtHTML)
    $('#' + elId).selectpicker()
    $('#' + elId  + ' > filter-option-inner-inner').html('Disease selection')
  })
}

/* both fields select */
// use for fetch ingredient only not support with other document
function helperMultiSelectPicker(elId, api, fieldValue1, fieldValue2, fieldValue3, fieldOption){
  //config request detail
  _load_selection_url = getHostAPI(api)
  _load_selection_headers = {'Content-Type': 'application/json; charset=utf-8'}
  // send get request
  _load_selection_response = axios.get(_load_selection_url, _load_selection_headers)
  // create options for select form
  _load_selection_response.then(function(res){
    let txtHTML = ''
    for(i=0; i < res.data.length; i++){
      txtHTML = txtHTML + '<option value="' + res.data[i][fieldValue1] + '-' + res.data[i][fieldValue2] + '-' + res.data[i][fieldValue3] +'">' + res.data[i][fieldOption] + '</option>'
    }
  // add response content to html element
    $('#' + elId).html(txtHTML)
    $('#' + elId).selectpicker()
    $('#' + elId  + ' > filter-option-inner-inner').html('Disease selection')
  })
}
