/* helper Post to lists */
function helperPostLists(data, apiURL){
    /* config request info */
    const _url = apiURL
    const _headers = {'Content-Type': 'application/json; charset=utf-8'}
    axios.post(_url, data)
    .then(function(res){
        console.log(res.data)
        tmpCreateList()
    })
    .catch(function(err){console.log(err)})
}

/* helper Delete item from lists */
function helperDelLists(_id, apiURL){
    let isConfirm = confirm('ยืนยันการลบ')
    if(isConfirm){
        /* config request info */
        const _url = apiURL + _id
        const _headers = {'Content-Type': 'application/json; charset=utf-8'}
        /* send request */
        axios.delete(_url, {id: _id})
        .then(function(res){
            console.log(res.data)
            tmpCreateList()
        })
        .catch(function(err){console.log(err)})
    }
}

/* helper Create item appent to table lists */
function helperCreateLists(el, keys, items, apiURL){
  let htmlResult = ''
  for(let i = 0; i < items.length; i++){
    let infoRow = ''
    let opnRow = '<tr><th scope="row">' + (i+1) + '</th>'
    let endRow = '<td><a style="color:red;" href="#del" onclick="helperDelLists(\'' + items[i]._id + '\', \'' + apiURL+ '\')"><span name="del"></span></a></td></tr>'
    for(key of keys){
        // infoRow = infoRow + '<td><textarea rows="1">' + items[i][key] + '</textarea></td>'
        infoRow = infoRow + '<td>' + items[i][key] + '</td>'
    }
    htmlResult = htmlResult + opnRow + infoRow + endRow
  }
  el.html(htmlResult)
  //    $('textarea').addClass('form-control')
  $('[name="del"]').addClass('fa fa-trash-o')
  $('#mainTable').DataTable({
    "paging": false,
    "scrollY": 530
  })
  $.fn.dataTable.ext.errMode = 'none';
}

/* helper custom Create item appent to table lists and first col must be img */
function helperCreateListsCustomImgFirst(el, keys, items, apiURL, imgSize){
  let htmlResult = ''
  for(let i = 0; i < items.length; i++){
    let infoRow = ''
    let opnRow = '<tr><th scope="row">' + (i+1) + '</th>'
    let endRow = '<td><a style="color:red;" href="#del" onclick="helperDelLists(\'' + items[i]._id + '\', \'' + apiURL+ '\')"><span name="del"></span></a></td></tr>'
    imgTicker = true
    for(key of keys){
        tdHTML = (imgTicker) ?'<td><img style="width:'+imgSize+';height:'+imgSize+'px;"  src="' + items[i][key] + '"></td>':'<td>' + items[i][key] + '</td>'
        infoRow = infoRow + tdHTML
        imgTicker = false
    }
    htmlResult = htmlResult + opnRow + infoRow + endRow
  }
  el.html(htmlResult)
  $('[name="del"]').addClass('fa fa-trash-o')
  $('#mainTable').DataTable({
    "paging": false,
    "scrollY": 530
  })
  $.fn.dataTable.ext.errMode = 'none';
}
