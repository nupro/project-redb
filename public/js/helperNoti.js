function helperNoti(objJQ, mode, message){
        let showTime = 500
        objJQ.css('visibility', 'visible')
        objJQ.toggleClass('alert-' + mode, true)
        objJQ.text(message)
        setTimeout(function(){
            setTimeout(function(){
                objJQ.css('visibility', 'hidden')
                objJQ.toggleClass('alert-' + mode, false)
            }, 100)
            $('#notiHelper').animate({opacity: 0})
        }, showTime)
}
