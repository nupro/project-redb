const helperURL = [
    'http://127.0.0.1:3030', // host api
    'http://127.0.0.1:8080'  // host web
    // 'http://staging.servs.in:3030', // host api
    // 'http://staging.servs.in:8080'  // host web
    // http://178.128.56.60:3030 // host api
    // http://178.128.56.60:8080 // host web
]

function getHostAPI(pathURL){
  return helperURL[0] + '/api/' + pathURL + '/'
}

function getHostWebApp(pathURL){
  return helperURL[1] + '/' + pathURL
}
