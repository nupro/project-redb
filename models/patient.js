/* patient */
const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');
const imgDefault = require('../helper/imgDefault')

const patientSchema = new mongoose.Schema({
    patient_id:{
        type: Number,
        unique: true,
        required: true
    },
    name_en: {
        type: String,
        unique: true,
        required: true
    },
    name_th: {
        type: String,
        unique: true,
        required: true
    },
    disease: {
        type: Array,
        required: true
    },
    disease_group: {
        type: String,
        required: true
    },
    doctor: {
        type: String,
        default: 'none'
    },
    admit_date: {
        type: Date,
        default: Date.now
    },
    created_user: {
        type: String,
        default: 'admin'
    },
    updated_user: {
        type: String,
        default: 'admin'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
})

const Patient = mongoose.model('Patient', patientSchema, 'Patient')

function validatePatient(patient) {
    const schema = {
        patient_id: Joi.number(),
        name_en: Joi.string(),
        name_th: Joi.string(),
        disease: Joi.array(),
        doctor: Joi.string().allow(''),
        admit_date: Joi.date().allow(''),
        disease_group: Joi.string(),
        /* info time stamp */
        updated_user: Joi.string().allow(''),
        updated_date: Joi.date().allow(''),
        created_user: Joi.string().allow(''),
        created_date: Joi.date().allow(''),
        /* other field from mongoose */
        $__: Joi.allow(''),
        isNew: Joi.allow(''),
        errors: Joi.allow(''),
        _doc: Joi.allow('')
    }
    return Joi.validate(patient, schema)
}

exports.Patient = Patient
exports.validatePatient = validatePatient
