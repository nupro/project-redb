/* foodlist */
const config = require('config');
const Joi = require('joi');
const mongoose = require('mongoose');

const foodlistSchema = new mongoose.Schema({
    foodlist_id: {
        type: Number,
        unique: true,
        required: true,
        default: null
    },
    name: {
        type: String,
        unique: true,
        required: true
    },
    detail: {
        type: String,
        default: 'no detail ... '
    },
    label: {
        type: String,
        unique: false
    },
    disease: {
        type: Array,
        default: []
    },
    menu: {
        type: Array,
        default: [],
        required: true
    },
    pointer: {
        type: String,
        default: 0
    },
    created_user: {
        type: String,
        required: true
    },
    updated_user: {
        type: String,
        required: true
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
})

const Foodlist = mongoose.model('Foodlist', foodlistSchema, 'Foodlist')

function validateFoodlist(foodlist) {
    const schema = {
        foodlist_id: Joi.number(),
        name: Joi.string(),
        detail: Joi.string().allow(''),
        //disease: Joi.array().allow([]),
        label: Joi.string().allow(''),
        menu: Joi.array(),
        pointer: Joi.string().allow(''),
        /* info time stamp */
        updated_user: Joi.string().allow(''),
        updated_date: Joi.date().allow(''),
        created_user: Joi.string().allow(''),
        created_date: Joi.date().allow(''),
        /* other field from mongoose */
        $__: Joi.allow(''),
        isNew: Joi.allow(''),
        errors: Joi.allow(''),
        _doc: Joi.allow('')
    }
    return Joi.validate(foodlist, schema)
}

exports.Foodlist = Foodlist
exports.validateFoodlist = validateFoodlist
