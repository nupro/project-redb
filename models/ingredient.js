/* Ingredient */
const config = require('config');
const Joi = require('joi');
const mongoose = require('mongoose');

const ingredientSchema = new mongoose.Schema({
    ingredient_id: {
        type: Number,
        unique: true,
        required: true,
        minlength: 1000
    },
    name_th: {
        type: String,
        unique: true,
        required: true
    },
    name_en: {
        type: String,
        unique: true
    },
    price: {
        type: Number,
        default: 0
    },
    name_unit: {
        type: String,
        required: true
    },
    created_user: {
        type: String,
        default: 'system'
    },
    updated_user: {
        type: String,
        default: 'system'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
})

const Ingredient = mongoose.model('Ingredient', ingredientSchema, 'Ingredient')

function validateIngredient(ingredient) {
    const schema = {
        ingredient_id: Joi.number(),
        name_th: Joi.string(),
        name_en: Joi.string(),
        name_unit: Joi.string(),
        price: Joi.number(),
        /* info time stamp */
        updated_user: Joi.string().allow(''),
        updated_date: Joi.date().allow(''),
        created_user: Joi.string().allow(''),
        created_date: Joi.date().allow(''),
        /* other field from mongoose */
        $__: Joi.allow(''),
        isNew: Joi.allow(''),
        errors: Joi.allow(''),
        _doc: Joi.allow('')
    }
    return Joi.validate(ingredient, schema)
}

exports.Ingredient = Ingredient
exports.validateIngredient = validateIngredient
