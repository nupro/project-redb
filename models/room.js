/* room */
const config = require('config');
const Joi = require('joi');
const mongoose = require('mongoose');

const roomSchema = new mongoose.Schema({
    room_id: {
        type: Number,
        unique: true,
        required: true,
        default: null
    }, patient_id: { type: Number,
        default: null
    },
    patient: {
        type: String,
        default: null
    },
    floor: {
        type: Number,
        required: true
    },
    admit_date: {
        type: Date,
        default: null
    },
    created_user: {
        type: String,
        required: true
    },
    updated_user: {
        type: String,
        required: true
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
})

const Room = mongoose.model('Room', roomSchema, 'Room')

function validateRoom(room) {
    const schema = {
        room_id: Joi.number(),
        patient_id: Joi.number(),
        patient: Joi.string(),
        floor: Joi.number(),
        admit_date: Joi.date(),
        updated_user: Joi.string(),
        updated_date: Joi.date(),
        created_user: Joi.string(),
        updated_user: Joi.string(),
        /* other field from mongoose */
        $__: Joi.allow(''),
        isNew: Joi.allow(''),
        errors: Joi.allow(''),
        _doc: Joi.allow('')
    }
    return Joi.validate(room, schema)
}

function validateRoomUpdate(user) {
    const schema = {
        room_id: Joi.number(),
        patient_id: Joi.number(),
        patient: Joi.string(),
        floor: Joi.number(),
        admit_date: Joi.date(),
        updated_user: Joi.string(),
        updated_date: Joi.date(),
        /* other field from mongoose */
        $__: Joi.allow(''),
        isNew: Joi.allow(''),
        errors: Joi.allow(''),
        _doc: Joi.allow('')
    }
    return Joi.validate(user, schema)
}
exports.Room = Room
exports.validateRoom = validateRoom
exports.validateRoomUpdate = validateRoomUpdate
