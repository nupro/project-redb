/* disease */
const config = require('config');
const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');

const diseaseSchema = new mongoose.Schema({
    disease_id: {
        type: Number,
        unique: true,
        required: true,
        default: null
    },
    name_th: {
        type: String,
        unique: true,
        required: true
    },
    name_en: {
        type: String,
        unique: true,
        required: true
    },
    ingre_heal: {
        type: Array,
        required: true,
        default: []
    },
    ingre_harm: {
        type: Array,
        required: true,
        default: []
    },
    created_user: {
        type: String,
        default: 'system'
    },
    updated_user: {
        type: String,
        default: 'system'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
})

const Disease = mongoose.model('Disease', diseaseSchema, 'Disease')

function validateDisease(disease) {
    const schema = {
        disease_id: Joi.number(),
        name_th: Joi.string(),
        name_en: Joi.string(),
        ingre_heal: Joi.array(),
        ingre_harm: Joi.array(),
        /* info time stamp */
        updated_user: Joi.string().allow(''),
        updated_date: Joi.date().allow(''),
        created_user: Joi.string().allow(''),
        created_date: Joi.date().allow(''),
        /* other field from mongoose */
        $__: Joi.allow(''),
        isNew: Joi.allow(''),
        errors: Joi.allow(''),
        _doc: Joi.allow('')
    }
    return Joi.validate(disease, schema)
}

exports.Disease = Disease
exports.validateDisease = validateDisease
