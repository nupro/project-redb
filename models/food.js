/* food */
const config = require('config');
const Joi = require('joi');
const mongoose = require('mongoose');
const imgDefault = require('../helper/imgDefault')

const foodSchema = new mongoose.Schema({
    food_id: {
        type: Number,
        unique: true,
        required: true,
        default: null
    },
    name_th: {
        type: String,
        required: true
    },
    name_en: {
        type: String,
        default: ''
    },
    type: {
        type: String,
        required: true
    },
    repast: {
        type: String,
        required: true
    },
    ban:{
        default: [],
        type: Array,
    },
    ingredient: {
        default: [],
        type: Array,
        required: true
    },
    created_user: {
        type: String,
        default: 'system'
    },
    updated_user: {
        type: String,
        default: 'system'
    },
    created_date: {
        type: Date,
        default: Date.now
    },
    updated_date: {
        type: Date,
        default: Date.now
    }
})

const Food = mongoose.model('Food', foodSchema, 'Food')

function validateFood(food) {
    const typeFood = [
        'main',
        'side'
    ]
    const typeRepast = [
        'breakfast',
        'lunch',
        'dinner'
    ]
    const schema = {
        food_id: Joi.number(),
        name_th: Joi.string(),
        name_en: Joi.string(),
        type: Joi.string().valid(typeFood),
        repast: Joi.string().valid(typeRepast),
        ingredient: Joi.array(),
        ban: Joi.array(),
        /* info time stamp */
        updated_user: Joi.string().allow(''),
        updated_date: Joi.date().allow(''),
        created_user: Joi.string().allow(''),
        created_date: Joi.date().allow(''),
        /* other field from mongoose */
        $__: Joi.allow(''),
        isNew: Joi.allow(''),
        errors: Joi.allow(''),
        _doc: Joi.allow('')
    }
    return Joi.validate(food, schema)
}

exports.Food = Food
exports.validateFood = validateFood
