/* foodlist cache*/
const config = require('config');
const Joi = require('joi');
const mongoose = require('mongoose');

const foodlistCacheSchema = new mongoose.Schema({}, { strict: false })

const FoodlistCache = mongoose.model('FoodlistCache', foodlistCacheSchema, 'FoodlistCache')

exports.FoodlistCache = FoodlistCache
