/* user */
//const config = require('config');
//const jwt = require('jsonwebtoken');
const Joi = require('joi');
const mongoose = require('mongoose');
const imgDefault = require('../helper/imgDefault')

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        min: 5,
        max: 60,
        trim: true,
        unique: true,
        required: true
    },
    password: {
        type: String,
        min: 4,
        trim: true
    },
    type: {
        type: String,
    },
    image: {
        type: String,
        trim: true,
        //default: 'no-image'
        default: imgDefault.userProfile
    },
    created_user: {
        type: String,
        trim: true,
        default: 'admin'
    },
    updated_user: {
        type: String,
        trim: true,
        default: 'admin'
    },
    created_date: {
        type: Date,
        trim: true,
        default: Date.now
    },
    updated_date: {
        type: Date,
        trim: true,
        default: Date.now
    }
})
const User = mongoose.model('User', userSchema, 'User')

function validateUser(user) {
    const userType = [
        'admin',
        'nurse',
        'nutritionist'
    ]
    const schema = {
        email: Joi.string().min(5).max(60),
        password: Joi.string().min(8),
        type: Joi.string().valid(userType),
        image: Joi.string().allow(''),
        /* info time stamp */
        updated_user: Joi.string().allow(''),
        updated_date: Joi.date().allow(''),
        created_user: Joi.string(),
        created_date: Joi.date().allow(''),
        /* other field from mongoose */
        $__: Joi.allow(''),
        isNew: Joi.allow(''),
        errors: Joi.allow(''),
        _doc: Joi.allow('')
    }
    return Joi.validate(user, schema)
}

function validateUserUpdate(user) {
    const userType = [
        'admin',
        'nurse',
        'nutritionist'
    ]
    const schema = {
        email: Joi.string().min(5).max(60),
        password: Joi.string().min(4),
        type: Joi.string().valid(userType),
        image: Joi.string().allow(''),
        /* info time stamp */
        updated_user: Joi.string().allow(''),
        updated_date: Joi.date().allow(''),
        created_user: Joi.string(),
        created_date: Joi.date().allow(''),
        /* other field from mongoose */
        $__: Joi.allow(''),
        isNew: Joi.allow(''),
        errors: Joi.allow(''),
        _doc: Joi.allow('')
    }
    return Joi.validate(user, schema)
}

exports.User = User
exports.validateUser = validateUser
exports.validateUserUpdate = validateUserUpdate
