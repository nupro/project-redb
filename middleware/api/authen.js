const jwt = require('jsonwebtoken')
const config = require('config')

async function authen (req, res, next){
    const token = req.header('x-auth-token')
    if(!token) res.status(401).send('No Authenticated')
    try{
        const decoded = await jwt.verify(token, config.get('jwtPrivateKey'))
        //req.user = decoded
        next()
    } catch(error) {
        res.status(400).send('Invalid token')
    }
}

module.exports = authen
