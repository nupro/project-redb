const jwt = require('jsonwebtoken')
const config = require('config')

async function isLogin (req, res, next){
    try{
        const decoded = await jwt.verify(req.cookies.token, config.get('jwtPrivateKey'))
        console.log(decoded)
    }catch(error){
        //console.log(error)
        res.status(200).render('pages/login')
    }
    next()
}

module.exports = isLogin
