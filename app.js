const config = require('config')
const express = require('express')
//const bodyParser = require("body-parser")
const cors = require('cors')
//const utf8 = require("utf8")

const {mongooseConnector} = require('./helper/mongooseConnector')

const user = require('./routes/api/user')
const patient = require('./routes/api/patient')
const ingredient = require('./routes/api/ingredient')
const foodlist = require('./routes/api/foodlist')
const food = require('./routes/api/food')
const disease = require('./routes/api/disease')
const authen = require('./routes/api/authen')
// const room = require('./api/room')
// const mobile = require('./api/mobile')

/* check configuration file */
// export NODE_ENV=develop
if (!config.get('jwtPrivateKey')){
    console.log('API need config variable')
    process.exit(1)
}

const app = express()

// CORS //
app.use(cors())

/* public path */
// app.use('/img', express.static('public'))

/* middleware */
app.use(express.json())
// app.use(bodyParser.urlencoded({
    // extended: true
// }))

/* API route path */
// app.use('/api/room', room)
//app.use('/api/mobile', mobile)
app.use('/api/user', user)
app.use('/api/patient', patient)
app.use('/api/foodlist', foodlist)
app.use('/api/food', food)
app.use('/api/disease', disease)
app.use('/api/ingredient', ingredient)
app.use('/api/authen', authen)

const port = process.env.API_PORT | 80
app.listen(port, () => console.log(`Listening on port ${port}...`))
